const webpack = require('webpack');
const htmlWebPackPlugin = require('html-webpack-plugin');
const path = require('path');

module.exports = {
	entry: [
		'react-hot-loader/patch',
		'./src/index.js',
	],
	module: {
		rules: [
			{
				test: /\.jsx?$/,
				exclude: /node_modules/,
				use: [
					{ loader: 'babel-loader' }
				],
			},
			{
				test: /\.html$/,
				use: [
					{
						loader: 'html-loader',
						options: { minimize : true }
					}
				]
			},
			{
				test: /\.css$/,
				use: [
					{ loader: 'style-loader' },
					{ loader: 'css-loader' },
				]
			}
		]
	},
	resolve:{
		extensions: ['.js', '.jsx']
	},
	output: {
		path: path.join(__dirname, '/dist'),
		publicPath: '/',
		filename: 'bundle.js'
	},
	plugins: [
		new webpack.HotModuleReplacementPlugin(),
		new htmlWebPackPlugin({
			template: './src/index.html',
			filename: './index.html'
		})
	],
	devServer: {
		contentBase: './dist',
		hot: true
	},
}
